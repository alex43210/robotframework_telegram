from threading import Thread, Lock
import time


class ThreadPool:
    def __init__(self, size, sleep_time):
        self._tasks = []
        self._tasks_lock = Lock()
        self._sleep_time = sleep_time
        for i in range(0, size):
            thread = Thread(target=self._body)
            thread.start()

    def _body(self):
        while True:
            with self._tasks_lock:
                if len(self._tasks) > 0:
                    task = self._tasks[0]
                    del self._tasks[0]
                else:
                    task = None
            if task is not None:
                task()
            else:
                time.sleep(self._sleep_time)

    def add_task(self, task):
        with self._tasks_lock:
            self._tasks.append(task)
