from robotframework import Application, Conversation, User, Robot
from sqlalchemy import Column, Integer
from sqlalchemy.exc import SQLAlchemyError
from telegram import User as TgUser, Bot as TgBot, Update
from telegram.ext import Updater, Filters,\
    CommandHandler, MessageHandler
from .thread_pool import ThreadPool, Lock


class RobotFrameworkTelegram:
    def __init__(self, config):
        """
        Initialize bot instances
        :param config: configuration dict
        :type config: dict
        """
        self.config = config
        self._check_config()
        self._add_extra_db_fields()
        self.application = Application(self.config)

        self.config = config
        self.logger = config['LOGGER']

        self.logger.info("Initialization")

        self.updaters = [Updater(token, workers=config['TELEGRAM_WORKERS_PER_BOT'])
                         for token in config['TOKENS']]

        self._telegram_output_preprocessors_lock = Lock()
        self._telegram_output_preprocessors = []

        self.thread_pool = ThreadPool(config['WORKER_COUNT'], config['WORKER_SLEEP_TIME'])
        start_handler = CommandHandler(config.get('START_COMMAND', 'start'),
                                       self.start_command_handler)
        message_handler = MessageHandler(Filters.text, self.message_handler)
        for updater in self.updaters:
            updater.dispatcher.add_handler(start_handler)
            updater.dispatcher.add_handler(message_handler)
        self.logger.info("Initialized")

    def _check_config(self):
        assert 'TOKENS' in self.config and \
            isinstance(self.config['TOKENS'], list) and \
            len(self.config['TOKENS']) > 0
        for item in self.config['TOKENS']:
            assert isinstance(item, str) and item != ""
        assert 'NO_ANSWER_MESSAGE' in self.config and \
            isinstance(self.config['NO_ANSWER_MESSAGE'], str)  and \
               self.config['NO_ANSWER_MESSAGE'] != ""
        assert 'NO_INSTANCE_MESSAGE' in self.config and \
               isinstance(self.config['NO_INSTANCE_MESSAGE'], str)  and \
               self.config['NO_INSTANCE_MESSAGE'] != ""
        assert 'TELEGRAM_WORKERS_PER_BOT' in self.config and \
            isinstance(self.config['TELEGRAM_WORKERS_PER_BOT'], int) and \
            self.config['TELEGRAM_WORKERS_PER_BOT'] >= 1
        assert 'WORKER_COUNT' in self.config and \
            isinstance(self.config['WORKER_COUNT'], int) and \
            self.config['WORKER_COUNT'] >= 1
        assert 'WORKER_SLEEP_TIME' in self.config and \
               (isinstance(self.config['WORKER_SLEEP_TIME'], int) or
                isinstance(self.config['WORKER_SLEEP_TIME'], float)) and \
               self.config['WORKER_SLEEP_TIME'] >= 0
        assert 'LOGGER' in self.config

    def add_telegram_output_preprocessor(self, func):
        """
        Add output preprocessing function
        :param func: preprocessing function. \
            Signature is (bot, user, chat_id, conversation, text)->processed_text
        :type func: (TgBot, TgUser, int, Conversation, str)->str
        """
        with self._telegram_output_preprocessors_lock:
            self._telegram_output_preprocessors.append(func)

    def remove_telegram_output_preprocessor(self, func):
        """
        Remove output preprocessing function
        :param func: preprocessing function. \
            Signature is (bot, user, chat_id, conversation, text)->processed_text
        :type func: (TgBot, TgUser, int, Conversation, str)->str
        """
        with self._telegram_output_preprocessors_lock:
            try:
                index = self._telegram_output_preprocessors.index(func)
                del self._telegram_output_preprocessors[index]
            except Exception:
                pass

    def _add_extra_db_fields(self):
        """
        Initialize extra SQLAlchemy fields specifical to telegram bots
        """
        Robot.telegram_bot_id = Column(Integer)
        Conversation.telegram_chat_id = Column(Integer)

    def _begin_conversation(self, bot_id, user_id, chat_id):
        """
        Initialize new conversation
        :param bot_id: telegram bot id
        :type bot_id: int
        :param user_id: user id
        :type user_id: int
        :param chat_id: telegram chat id
        :type chat_id: int
        :return: new conversation
        :rtype: Conversation
        """
        self.logger.info("Starting new conversation")
        robot = Robot.filter(
            lambda q: q.where(Robot.telegram_bot_id == bot_id),
            lambda q: q.limit(1)
        )[0]
        username = "telegram{0}".format(user_id)
        users = User.filter(
            lambda q: q.where(User.name == username),
            lambda q: q.limit(1)
        )
        if len(users) == 0:
            user = User(name=username)
            self.application.session.add(user)
            self.application.session.commit()
        else:
            user = users[-1]
        conversation = Conversation(robot_id=robot.id,
                                    user_id=user.id,
                                    telegram_chat_id=chat_id)
        self.application.session.add(conversation)
        self.application.session.commit()
        return conversation

    def _handle(self, handler):
        """
        Add handler function (command/message) to task queue \
            and run it when one of worker threads will be free
        :param handler: ()->NoneType
        """
        def _wrapper():
            try:
                handler()
            except SQLAlchemyError as err:
                self.logger.error("SQL exception : {0}".format(str(err)))
                self.application.session.flush()
            except Exception as err:
                self.logger.error("Exception : {0}".format(str(err)))
        self.thread_pool.add_task(_wrapper)

    def _output(self, user, chat_id, bot, conversation, text):
        """
        Send output
        :param user: user
        :type user: User
        :param chat_id: chat id
        :type chat_id: int
        :param bot: bot
        :type bot: Bot
        :param conversation: conversation
        :type conversation: Conversation
        :param text: text (before preprocessing)
        :type text: str
        """
        with self._telegram_output_preprocessors_lock:
            for preprocessor in self._telegram_output_preprocessors:
                text = preprocessor(bot, user, chat_id, conversation, text)
        self.logger.info("Sending answer {0}".format(text))
        bot.send_message(chat_id=str(chat_id), text=text)

    def _conversation_bot_instantiated_check(self, chat_id, conversation, bot):
        """
        Send message to user if conversation "target" robot not instantiated now.
        :param chat_id: chat id
        :type chat_id: int
        :param conversation: conversation
        :type conversation: Conversation
        :param bot: telegram bot
        :type bot: Bot
        """
        if conversation.robot.id not in Robot._instances:
            bot.send_message(chat_id=str(chat_id),
                             text=self.config['NO_INSTANCE_MESSAGE'])

    def start_command_handler(self, bot, update):
        """
        Handle "/start" command (to begin new conversation)
        :param bot: telegram bot
        :type bot: Bot
        :param update: update
        :type update: Update
        """
        def _handler():
            conversation = self._begin_conversation(
                bot.id,
                update.message.from_user.id,
                update.message.chat_id
            )
            self._conversation_bot_instantiated_check(
                update.message.chat_id,
                conversation, bot
            )
            item = conversation.output()
            self._output(update.message.from_user, update.message.chat_id,
                         bot,
                         conversation,
                         item.text)
        self._handle(_handler)

    def message_handler(self, bot, update):
        """
        Handle message
        :param bot: telegram bot
        :type bot: Bot
        :param update: update
        :type update: Update
        """
        def _handler():
            conversations = Conversation.filter(
                lambda q: q.where(Conversation.telegram_chat_id == update.message.chat_id),
                lambda q: q.order_by(Conversation.id.desc()).limit(1)
            )
            if len(conversations) == 0:
                conversation = self._begin_conversation(
                    bot.id,
                    update.message.from_user.id,
                    update.message.chat_id
                )
                bot.sendMessage(chat_id=update.message.chat_id,
                                text=conversation.output().text)
            else:
                conversation = conversations[0]
            self._conversation_bot_instantiated_check(
                update.message.chat_id,
                conversation, bot
            )
            self.logger.info("Getter request {0}".format(update.message.text))
            answer = conversation.answer(update.message.text)
            if not answer.stage:
                text=self.config['NO_ANSWER_MESSAGE']
            else:
                text=answer.text
            self._output(update.message.from_user, update.message.chat_id,
                         bot,
                         conversation,
                         text)
        self._handle(_handler)

    def start_polling(self):
        """
        Run bot
        """
        for updater in self.updaters:
            updater.start_polling()
