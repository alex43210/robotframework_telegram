from robotframework_telegram import RobotFrameworkTelegram
from config import config


if __name__ == '__main__':
    bot = RobotFrameworkTelegram(config)
    bot.start_polling()
