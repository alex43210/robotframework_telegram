import logging


config = {
    'DATABASE_URI': 'postgresql://postgres:6xaz8m4r@localhost:5432/weatherbot',
    'KEEP_WORD_VECTORS': 3,
    'ROBOT_CONFIGURATIONS': 'robot_configurations',
    'KEEP_ROBOTS': 5,
    'OUTPUT_HANDLERS': {},
    'TOKENS': [
        # Your tokens here
    ],
    'NO_ANSWER_MESSAGE': 'Sorry, I can\'t answer now.',
    'NO_INSTANCE_MESSAGE': 'Sorry, robot not instantiated yet. ' + \
                           'You\'ll get answer after instantiation - it can take few minutes',
    'TELEGRAM_WORKERS_PER_BOT': 1,
    'WORKER_COUNT': 2,
    'WORKER_SLEEP_TIME': 0.2,
    'LOGGER': logging,
}
