Robotframework-telegram
-----------------------
This is a wrapper for my [robotframework](https://bitbucket.org/alex43210/robotframework) that allows to use it with Telegram.
You can see example here:
```
from robotframework_telegram import RobotFrameworkTelegram
from config import config


if __name__ == '__main__':
    bot = RobotFrameworkTelegram(config)
    bot.start_polling()
```